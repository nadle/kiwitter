from asgiref.sync import async_to_sync
from channels.generic.websocket import JsonWebsocketConsumer
from django.utils import timezone

from users.models import User
from .models import Conversation, Message


class ChatConsumer(JsonWebsocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = None
        self.participants = []

    def connect(self):
        self.accept()
        # print("WebSocket Connection Established")
        self.conver_id = self.scope["url_route"]["kwargs"]["converId"]
        async_to_sync(self.channel_layer.group_add)(self.conver_id, self.channel_name)


    def receive_json(self, content, **kwargs):
        username = content.get("username", "Anonymous")
        user = User.objects.get(username=username)
        message = content.get("message", "")
        conversation, created = Conversation.objects.get_or_create(id=self.conver_id)
        new_message = Message.objects.create(conversation=conversation,  sender=user, content=message)
        # last_message_at 필드를 현재 시간으로 업데이트합니다.
        conversation.last_message_at = timezone.now()
        conversation.save()

        # 클라이언트에 전송할 메시지 포맷을 맞춤
        new_message_json = {
            "id": new_message.id,  # 메시지 ID 추가
            "sender": username,  # sender의 username 직접 할당
            "content": message,  # 메시지 내용
            "timestamp": new_message.timestamp.strftime("%Y-%m-%d %H:%M:%S")  # 시간 포맷 맞춤
        }

        # 새 메시지만 클라이언트에 전송합니다.
        async_to_sync(self.channel_layer.group_send)(
            self.conver_id,
            {
                'type': 'chat_message',
                'message': new_message_json
            }
        )
        
        
    def chat_message(self, event):
        print("chat_message 메소드 진입")
        print("event : ", event)
        self.send_json(event['message'])


    def disconnect(self, close_code):
        # print("WebSocket Connection Closed")
        async_to_sync(self.channel_layer.group_discard)(
            self.conver_id,
            self.channel_name
        )